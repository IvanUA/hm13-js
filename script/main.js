//    1 
// setTimeout дозволяє викликати функцію один раз через певний інтервал часу. 
// setInterval дозволяє викликати функцію регулярно, повторюючи виклик через певний 
// проміжок часу.

//    2
// Якщо ви використовуєте нульову затримку у функції setTimeout(), код буде виконано якомога швидше, 
// але виконання може затриматися, якщо є інші події або операції в черзі.

//    3
// Функція clearInterval() використовується для припинення виконання повторюваного запуску функції 
// або виконання коду через заданий інтервал часу. Якщо ви не викличете clearInterval(), цикл запуску 
// буде продовжуватися і виконувати код за заданим інтервалом, що може мати негативні наслідки.

// Ось декілька причин, чому важливо не забувати викликати clearInterval():

// Заощадження ресурсів: Якщо цикл запуску продовжується безперервно, він буде виконувати код у 
// фоновому режимі навіть тоді, коли це вам вже не потрібно. Це може призвести до непотрібного витрачання 
// обчислювальних ресурсів, енергії й батареї, особливо на пристроях з обмеженими ресурсами.

// Уникнення непередбачених проблем: Якщо ви виконуєте певний код у циклі запуску, який залежить 
// від певних умов або контексту, непередбачений запуск цього коду може викликати проблеми. Наприклад, 
// якщо ви оновлюєте сторінку або виходите зі сторінки, але цикл запуску продовжується, код може
//  спрацювати в недоречний момент і створити небажану поведінку.

// Уникнення перевантаження сервера: Якщо ваша програма виконується на сервері і викликає 
// запити до сервера у циклі запуску, необхідно припинити цикл, коли вам вже не потрібні нові запити. 
// В іншому випадку цикл запуску може створювати надмірне навантаження на сервер, зменшувати продуктивність 
// і може навіть призвести до відмови в обслуговуванні (DoS).

let slideIndex = 0;
showSlides();


function showSlides() {
  let i;
  let slides = document.getElementsByClassName("image-to-show");
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  slideIndex++;
  if (slideIndex > slides.length) {slideIndex = 1}
  slides[slideIndex-1].style.display = "block";
  }

let IntervalId = setInterval(showSlides,3000);

function stopSlideshow() {
  clearInterval(IntervalId);
};

function resumeSlideshow() {
  showSlides();
  stopSlideshow()
};

document.getElementById('stopButton').addEventListener('click', stopSlideshow);
document.getElementById('resumeButton').addEventListener('click', resumeSlideshow);













//////////////////////////////////

// let image1 = document.querySelector("#image1");
// let image2 = document.querySelector("#image2");
// let image3 = document.querySelector("#image3");
// let image4 = document.querySelector("#image4");

// let images = [];
// images.push(image1, image2, image3, image4);

// let currentIndex = 0;
// let intervalId;

// function changeImage() {
//     let imageDiv = document.getElementById('image');
//     imageDiv.style.backgroundImage = 'url(' + images[currentIndex] + ')';
//     currentIndex = (currentIndex + 1) % images.length;
// }

// function startSlideshow() {
//     intervalId = setInterval(changeImage, 3000);
// }

// function stopSlideshow() {
//     clearInterval(intervalId);
// }

// function resumeSlideshow() {
//     startSlideshow();
//     changeImage();
// }

// document.getElementById('startButton').addEventListener('click', startSlideshow);
// document.getElementById('stopButton').addEventListener('click', stopSlideshow);
// document.getElementById('resumeButton').addEventListener('click', resumeSlideshow);


